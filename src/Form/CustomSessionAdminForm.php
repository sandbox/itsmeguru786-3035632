<?php

namespace Drupal\session_expire\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CustomSessionAdminForm.
 */
class CustomSessionAdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'session_expire.customsessionadmin',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_session_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('session_expire.customsessionadmin');

    $form['session_expire_desc'] = [
      '#type'  => 'markup',
      '#value' => t('This module requires cron to be correctly configured and running for Drupal.'),
    ];

    $options_array = [
      0,
      7200,
      10800,
      21600,
      43200,
      86400,
      172800,
      259200,
      604800,
    ];
    $interval = $this->build_options($options_array);
    $interval['0'] = t('Everytime');

    $form['session_expire_interval'] = [
      '#type' => 'select',
      '#title' => $this->t('Interval'),
      '#description' => $this->t('Run the cleanup at the specified interval. This tells Drupal how often to run the cleanup. On a busy site, you want that to be more frequent (e.g. every day at a minimum). You don\&#039;t want it to be too frequent though (e.g. every hour), as it can tie up the sessions table for a long time. Cron must be configured to run more frequently than the value you chose here.'),
      '#options' => $interval,
      '#size' => 1,
      '#default_value' => $config->get('session_expire_interval') ? $config->get('session_expire_interval') : 86400,
    ];

    $options_array = [
      1800,
      3600,
      7200,
      10800,
      21600,
      43200,
      86400,
      172800,
      259200,
      604800,
      1209600,
      2419200,
    ];
    $period = $this->buildOptions($options_array);
    $period['1000000000'] = t('Never');

    $form['session_expire_age'] = [
      '#type' => 'select',
      '#title' => $this->t('Age'),
      '#description' => $this->t('Expire sessions that are older than the specified age. Older entries will be discarded.'),
      '#options' => $period,
      '#size' => 1,
      '#default_value' => $config->get('session_expire_age') ? $config->get('session_expire_age') : 604800,
    ];

    $form['session_expire_mode'] = [
      '#type'          => 'radios',
      '#title'         => t('Session types'),
      '#default_value' => $config->get('session_expire_mode') ? $config->get('session_expire_mode') : 0,
      '#options'       => [
        t('Anonymous'),
        t('Both anonymous and authenticated users'),
      ],
      '#description'   => t('Types of sessions to discard. This option indicates whether only anonymous users, or both anonymous and authenticated users are expired. Note that if you choose authenticated users, they will be logged off and have to login again after the "age" specified above.'),
    ];

    $form['session_expire_last'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Last Expired Run'),
      '#default_value' => $config->get('session_expire_last') ? $config->get('session_expire_last') : 0,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('session_expire.customsessionadmin')
      ->set('session_expire_interval', $form_state->getValue('session_expire_interval'))
      ->save();
  }

  /**
   * Utility function.
   */
  public function buildOptions(array $time_intervals, $granularity = 2, $langcode = NULL) {
    $callback = function ($value) use ($granularity, $langcode) {
      return \Drupal::service('date.formatter')->formatInterval($value, $granularity, $langcode);
    };

    return array_combine($time_intervals, array_map($callback, $time_intervals));
  }

}
